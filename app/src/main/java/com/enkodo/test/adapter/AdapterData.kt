package com.enkodo.test.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enkodo.test.R
import com.enkodo.test.model.BrastlewarkData
import com.squareup.picasso.Picasso
import java.lang.String
import java.util.*


class AdapterData(data: ArrayList<BrastlewarkData>) :


    RecyclerView.Adapter<AdapterData.MyViewHolder>() {
    private val dataSet: ArrayList<BrastlewarkData> = data

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView = itemView.findViewById(R.id.name_card)
        var age: TextView = itemView.findViewById(R.id.age_card)
        var image: ImageView = itemView.findViewById(R.id.imageview_card)
        var height: TextView = itemView.findViewById(R.id.h_card)
        var weight: TextView = itemView.findViewById(R.id.w_card)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.cards_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val name = holder.name
        val age = holder.age
        val image = holder.image
        val weight = holder.weight
        val height = holder.height

        name.text = String.format("Name: %s", dataSet[listPosition].name)
        age.text = String.format("Age: %s", dataSet[listPosition].age)
        weight.text = String.format("Weight: %s", dataSet[listPosition].weight)
        height.text = String.format("Height: %s", dataSet[listPosition].height)

        Picasso.get()
            .load(dataSet[listPosition].thumbnail)
            .resize(50, 50)
            .centerCrop()
            .into(image)

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

}