package com.enkodo.test.model

class BrastlewarkData(
    val id: Int,
    val name: String,
    val thumbnail: String,
    val age: Int,
    val weight: Double,
    val height: Double,
    val hair_color: String
)