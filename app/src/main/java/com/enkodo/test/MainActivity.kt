package com.enkodo.test

import android.os.Bundle
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.enkodo.test.adapter.AdapterData
import com.enkodo.test.model.BrastlewarkData
import com.enkodo.test.service.BrastlewarkService
import com.enkodo.test.service.BrastlewarkServiceCallback
import com.kaopiz.kprogresshud.KProgressHUD
import es.dmoral.toasty.Toasty
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class MainActivity : AppCompatActivity(), BrastlewarkServiceCallback {

    private lateinit var responseArray: JSONArray
    private var mMessageTxt: TextView? = null
    private var hud: KProgressHUD? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: RecyclerView.Adapter<*>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var brastlewarkDataModels: ArrayList<BrastlewarkData>? = null
    private var mCriterioAct: AutoCompleteTextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mMessageTxt = findViewById(R.id.message)
        mCriterioAct = findViewById<AutoCompleteTextView>(R.id.criterio)
        recyclerView = findViewById(R.id.rv_data)
        layoutManager = LinearLayoutManager(this)

        init()

        findViewById<View>(R.id.go).setOnClickListener {
            order(mCriterioAct!!.text.toString())
        }
    }


    private fun init() {
        hud = KProgressHUD.create(this@MainActivity)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait...")

        recyclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()


        val brastlewarkService = BrastlewarkService()
        brastlewarkService.getData(this)

    }


    private fun order(str: String) {
        if (responseArray.length() > 0) {
            mMessageTxt!!.visibility = View.GONE
            recyclerView!!.visibility = View.VISIBLE

            brastlewarkDataModels = ArrayList<BrastlewarkData>()

            for (i in 0 until responseArray.length()) {
                val jsonObject = responseArray.getJSONObject(i)

                val id = jsonObject.getInt("id")
                val name = jsonObject.getString("name")
                val thumbnail = jsonObject.getString("thumbnail")
                val age = jsonObject.getInt("age")
                val weight = jsonObject.getDouble("weight")
                val height = jsonObject.getDouble("height")
                val hairColor = jsonObject.getString("hair_color")



                if (str == "") {
                    brastlewarkDataModels!!.add(
                        BrastlewarkData(id, name, thumbnail, age, weight, height, hairColor)
                    )
                } else {
                    if (name.toLowerCase().contains(str.toLowerCase())) {
                        brastlewarkDataModels!!.add(
                            BrastlewarkData(id, name, thumbnail, age, weight, height, hairColor)
                        )
                    }
                }
            }
            adapter = AdapterData(brastlewarkDataModels!!)
            recyclerView!!.adapter = adapter


        } else {
            mMessageTxt!!.visibility = View.VISIBLE
            recyclerView!!.visibility = View.GONE
        }
    }


    override fun onSuccess(response: Response?) {
        runOnUiThread {
            hud!!.show()
            try {
                val res = response!!.body!!.string()
                val json = JSONObject(res)
                responseArray = json.getJSONArray("Brastlewark")
                order("")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            hud!!.dismiss()
        }
    }

    override fun onError(code: Int, message: String?) {
        runOnUiThread {
            hud!!.dismiss()
            Toasty.warning(
                this@MainActivity,
                applicationContext.resources.getString(R.string.fallo_service) + " Code: " + code
            ).show()
        }
    }

    override fun onFailure() {
        runOnUiThread {
            hud!!.dismiss()
            Toasty.error(
                this@MainActivity,
                applicationContext.resources.getString(R.string.error_service)
            ).show()
        }
    }
}