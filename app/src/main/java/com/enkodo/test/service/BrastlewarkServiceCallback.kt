package com.enkodo.test.service

import okhttp3.Response

interface BrastlewarkServiceCallback {
    fun onSuccess(response: Response?)

    fun onError(code: Int, message: String?)

    fun onFailure()
}