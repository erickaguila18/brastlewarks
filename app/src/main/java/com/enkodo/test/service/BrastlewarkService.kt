package com.enkodo.test.service

import android.util.Log
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class BrastlewarkService : IBrastlewarkService, Callback {
    private var mCallback: BrastlewarkServiceCallback? = null

    override fun getData(callback: BrastlewarkServiceCallback?) {
        mCallback = callback

        val request: Request = Request.Builder()
            .url("https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json")
            .get()
            .build()
        val c: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(59, TimeUnit.SECONDS)
            .readTimeout(59, TimeUnit.SECONDS)
            .build()
        c.newCall(request).enqueue(this)
    }

    override fun onFailure(call: Call, e: IOException) {
        Log.e(TAG, "onFailure: ", e)
        mCallback!!.onFailure()
    }

    override fun onResponse(call: Call, response: Response) {
        if (!response.isSuccessful) {
            Log.e("Services", "onResponse error" + response.code + " " + response.message)
            mCallback!!.onError(response.code, response.message)
        } else {
            Log.e("Services", "onSuccess")
            mCallback!!.onSuccess(response)
        }
    }

    companion object {
        private val TAG = BrastlewarkService::class.java.simpleName
    }
}