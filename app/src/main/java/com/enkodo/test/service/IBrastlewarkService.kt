package com.enkodo.test.service

interface IBrastlewarkService {
    fun getData(
        callback: BrastlewarkServiceCallback?
    )
}